package ru.tsc.anaumova.tm.model;

import ru.tsc.anaumova.tm.constant.ArgumentConst;
import ru.tsc.anaumova.tm.constant.TerminalConst;

public class Command {

    public static Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "Show system info."
    );

    public static Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "Show developer info."
    );

    public static Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Show application version."
    );

    public static Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Show terminal commands."
    );

    public static Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Close application."
    );

    private String name = "";

    private String argument = "";

    private String description = "";

    public Command() {
    }

    public Command(final String name) {
        this.name = name;
    }

    public Command(String name, String argument) {
        this.name = name;
        this.argument = argument;
    }

    public Command(String name, String argument, String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String description) {
        this.argument = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}
